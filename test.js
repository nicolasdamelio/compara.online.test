'use strict';

// This class is used for logins
class Login {
	constructor(hash) {
		this.sessions = [];
		this.users = [];
		this.passwords = [];
		Object.keys(hash).map(k => ({k, v: hash[k]})).map(e => {
			this.users = this.users.concat([e.k]);
			this.passwords = this.passwords.concat([e.v]);
		});
	}

	logout(user) {
		let index = 0;
		for (let i of this.sessions) {
			if (i === user) {
				this.sessions[index] = null;
				this.sessions = this.sessions.filter(session => session !== null);
				return true;
			}
			index++;
		}
		return false;
	}

	// Checks if user exists
	userExists(user) {
		for (let i of this.users) {
			if (i === user) {
				return true;
			}
		}
		return false;
	}

	// Register user
	registerUser(user, password) {
		if (!this.userExists(user)){
			this.users.push(user);
			this.passwords.push(password);
		}
	}

	removeUser(user) {
		let index = this.idx(user, this.users);
		this.users[index] = null;
		this.passwords[index] = null;
		this.users = this.users.filter(user => user !== null);
		this.passwords = this.passwords.filter(password => password !== null);
	}

	checkPassword(user, password) {
		let index = this.idx(user, this.users);
		return (this.passwords[index] === password);
	}

	updatePassword(user, oldPassword, newPassword) {
		// First we check if the user exists
		let index = 0;
		for (let i of this.users) {
			if (i === user && this.passwords[index] === oldPassword) {
				this.passwords[index] = newPassword;
				return true;
			}
			index++;
		}
		return false;
	}

	login(user, password) {
		// First we check if the user is in session right now.
		for (let i of this.sessions) {
			if (i === user) {
				return false;
			}
		}

		let index = this.idx(user, this.users);
		if (this.passwords[index] === password) {
			this.sessions.push(user);
			return true;
		}
	}

	// Gets index of an element in an array
	idx(element, array) {
		let cont=0;
		for (let i of array) {
			if (i === element) {
				return cont;
			}
			cont += 1;
		}
		return cont;
	}
}


let registeredUsers = {
	user1: 'pass1',
	user2: 'pass2',
	user3: 'pass3'
};

let login = new Login(registeredUsers);
console.log(login.users);
login.registerUser('user4', 'pass4');
console.log(login.users);
console.log(login.sessions);
login.updatePassword('user3', 'pass5', 'pass5');
login.updatePassword('user3', 'pass3', 'pass5');
login.login('user1', 'pass1');
login.login('user2', 'pass2');
login.login('user3', 'pass5');
login.login('user3', 'pass5');
login.login('user3', 'pass5');
login.login('user3', 'pass5');
console.log(login.sessions);
login.logout('user4');
login.logout('user3');
console.log(login.sessions);